<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware(['visit'])->group(function(){
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/register','AuthController@register');
    Route::post('auth/password-reset/email', 'AuthController@sendResetLinkEmail');
    Route::get('auth/password-reset/{token}', 'AuthController@getToken');
    Route::post('auth/password-reset', 'AuthController@resetPassword');
    Route::get('auth/verify-account/{email}','AuthController@verifyAccount');

    Route::get('auth/{social}','SocialAuthController@handleProviderCallback');
    Route::get('auth/redirect/{social}', 'SocialAuthController@redirect');

    Route::middleware(['token'])->group(function () {
        Route::post('auth/logout', 'AuthController@logout');
        Route::get('house/favorite', 'HouseController@favoriteHouseList');
        Route::post('house/favorite/{id}', 'HouseController@favoriteHouse');
        Route::resource('house', 'HouseController');
        Route::resource('house/{id}/report', 'ReportController')->only([
            'store', 'destroy'
        ]);
        Route::get('dashboard', 'UsersController@siteVisitors');
        Route::resource('members', 'UsersController');
        Route::get('members/{id}/attempt', 'UsersController@loginAsMember');
        Route::post('members/admin', 'UsersController@addAdmin');
        // Route::post('users/my_account/{id}', 'UsersController@update');
        Route::get('admin/reports', 'ReportController@allReports');
        Route::post('admin/reports/{id}/answer', 'ReportController@answerReport');
    });
});