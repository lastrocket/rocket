<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'facebook' => [ 
        'client_id' => '407674072971619',
        'client_secret' => '3ccb1a40a5c989aca6a9edfe15605893',
        'redirect' => 'https://summerhouse.loc/api/auth/redirect/facebook'
    ],
    
    'google' => [ 
        'client_id' => '18676824894-tglcnj8e8cn6h1mroeg6ml42c05dlvn6.apps.googleusercontent.com',
        'client_secret' => 'zltLiKGa7togUZm1H7I2cKj9',
        'redirect' => 'https://summerhouse.loc/api/auth/redirect/google'  
    ],

];