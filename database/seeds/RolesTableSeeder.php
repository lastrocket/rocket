<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		'id' => 1,
        		'role' => 'SuperAdmin',
        		'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
        	],
        	[
        		'id' => 2,
        		'role' => 'Admin',
        		'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
        	],
        	[
        		'id' => 3,
        		'role' => 'User',
        		'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
        	]
        ];
        Role::insert($data);
    }
}
