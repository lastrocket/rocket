<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('house_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('house_images', function (Blueprint $table) {
            $table->foreign('house_id')->references('id')->on('houses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('house_images', function (Blueprint $table) {
            $table->dropForeign('house_images_house_id_foreign');
        });
        Schema::dropIfExists('house_images');
    }
}
