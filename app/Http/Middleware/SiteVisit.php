<?php

namespace App\Http\Middleware;

use Visitor;
use Closure;
use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class SiteVisit extends Middleware 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Visitor::log();
        return $next($request);
    }
}
