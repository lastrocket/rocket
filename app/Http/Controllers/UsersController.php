<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Contracts\UserServiceInterface;
use JWTAuth;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\MailRequest;

class UsersController extends Controller
{
    public function index(UserServiceInterface $userService) 
    {
        $users = $userService->getAllUsers();
        return response()->json(['success' => true, 'users' => $users], 201);       
    }

    public function addAdmin(MailRequest $request,  UserServiceInterface $userService) 
    {
        $inputs = $request->all();
        $admin = $userService->addAdminByUserEmail($inputs);
        if($admin) {
            return response()->json(['success' => true, 'admin' => $admin], 201);
        }
        return response()->json(['success' => false, 'data' => 'Error'], 500);
    }

    public function loginAsMember($id, UserServiceInterface $userService) 
    {
        $token = $userService->getMembersToken($id);
        if($token){
            return response()->json(compact('token', 'user'), 200);
        }
        return response()->json(['success' => false], 500);
    }

    public function store(RegisterRequest $request, UserServiceInterface $userService) 
    {
        $inputs = $request->all();
        $user   = $userService->createUser($inputs);
        if($user) {
            return response()->json(['success' => true, 'user' => $user], 201);
        }
        return response()->json(['success' => false, 'data' => 'Error'], 500);
    }

    public function show($id, UserServiceInterface $userService) 
    {
        $user = $userService->getUserById($id);
        if($user) {
            return response()->json(['success' => true, 'user' => $user], 201);
        }
        return response()->json(['success' => false, 'data' => 'Error'], 500);
    }

    public function update($id, UpdateUserRequest $request, UserServiceInterface $userService)
    {
        $input = $request->except(['_method']);
        $image = '';
        if(isset($input['image'])) {
            $image = $input['image'];
        }
        $result = $userService->update($id, $input, $image);
        if($result){
            return response()->json(['success' => true], 201);
        }
        return response()->json(['success' => false, 'data' => 'Error'], 500);
    }

    public function destroy($id, UserServiceInterface $userService)
    {
        $user = $userService->deleteUserById($id);
        if($user) {
            return response()->json(['success' => true, 'user' => $user], 201);
        }
        return response()->json(['success' => false, 'data' => 'Error'], 500);
    }

    public function siteVisitors(UserServiceInterface $userService)
    {
        $result = $userService->countPerDay();
        return response()->json(compact('result'), 201);
    }
    
}
