<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTFactory;
use JWTAuth;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Contracts\AuthServiceInterface;

class AuthController extends Controller
{
    public function login(LoginRequest $request, AuthServiceInterface $authService)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = $authService->getUserByEmail($credentials['email']);
        Auth::login($user);
        return response()->json(compact('token', 'user'));
    }

    public function register(RegisterRequest $request, AuthServiceInterface $authService)
    {
        $inputs = $request->all();
        $user = $authService->register($inputs);
        
        if($user) {
            Auth::login($user);
            $token = JWTAuth::fromUser($user);
            $authService->verification($user['email']);
            return response()->json(['success' => true, 'data' => compact('token', 'user')], 201);
        }
        return response()->json(['success' => false, 'data' => 'Error'], 500);
    }

    public function sendResetLinkEmail(Request $request, AuthServiceInterface $authService)
    {
        $this->validate($request, ['email' => 'required|email']);
       
        $inputs = $request->only('email');
        $result = $authService->sendResetLinkEmail($inputs); 
        if($result){
            return response()->json(['success' => true], 201);
        }
        return response()->json(['success' => false, 'data' => 'Error'], 500);
        
    }

    public function getToken($token, AuthServiceInterface $authService) {
        $result = $authService->checkToken($token);
        if($result){
            return response()->json(['success' => true], 201);
        }
        return response()->json(['success' => false, 'data' => 'Error'], 500);
    }

    public function resetPassword(Request $request, AuthServiceInterface $authService)
    {
        $this->validate($request, ['password' => 'required']);
        $inputs = $request->all();
        $result = $authService->resetPassword($inputs);
        if($result){
            return response()->json(['success' => true], 201);
        }
        return response()->json(['success' => false, 'data' => 'Error'], 500);
    }

    public function verifyAccount($email, AuthServiceInterface $authService)
    {
        $result = $authService->verifyAccount($email);
        if($result){
            return response()->json(['success' => true], 201);
        }
        return response()->json(['success' => false], 500);

    }

    public function logout()
    {
        if(JWTAuth::invalidate()){
            return response()->json([
                    'status' => 'success',
                    'data' => 'Logged out Successfully.'
                ], 200);
        }
        return response()->json(['status' => 'error', 'data' => 'Logged out Failed'], 500);
    }
}