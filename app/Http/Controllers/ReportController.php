<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\ReportServiceInterface;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    public function allReports(ReportServiceInterface $reportService){
        $reports = $reportService->getAllReports();
        return response()->json(['success' => true, 'report' => $reports], 200);
    }

    public function answerReport($id, Request $request, ReportServiceInterface $reportService) {
        $input = $request->all();
        $report = $reportService->answerReport($id, $input);
        if($report) {
            return response()->json(['success' => true, 'report' => $report]);
        }
        return response()->json(['success' => false], 500);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request, ReportServiceInterface $reportService)
    {
        $inputs = $request->all();
        $inputs['house_id'] = $id;
        $inputs['user_id'] = Auth::id();
        $report = $reportService->addReport($inputs);
        if($report) {
            return response()->json(['success' => true, 'report' => $report], 200);
        }
        return response()->json(['success' => false], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($house_id, $id, ReportServiceInterface $reportService)
    {
        $report = $reportService->delete($id);
        if($report) {
            return response()->json(['success' => true, 'report' => $report], 200);
        }
        return response()->json(['success' => false], 500);
    }
}
