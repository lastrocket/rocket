<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\User;
use App\Contracts\AuthServiceInterface;

class SocialAuthController extends Controller
{
    /**
     * Create a redirect method to social api.
     *
     * @return void
     */
    
    public function redirect($social)
    {
        return Socialite::driver($social)->redirect();       
    } 

    public function handleProviderCallback($social, AuthServiceInterface $service)
    {
        $user = $service->createOrGetUser($social, Socialite::driver($social)->user());
     
        // auth()->login($user);

        return response()->json(['success' => true, 'data' => 'Create account'], 201);
            
    }
     
}
