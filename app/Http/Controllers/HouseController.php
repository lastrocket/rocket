<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\HouseServiceInterface;
use Illuminate\Support\Facades\Auth;

class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HouseServiceInterface $houseService)
    {
        $lists = $houseService->getAll();
        return response()->json(compact('lists'), 200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, HouseServiceInterface $houseService)
    {
        $inputs = $request->except(['image']);
        $images = $request['image'];
        $result = $houseService->add($inputs,$images);
        if($result) {
            return response()->json(compact('inputs'), 201);
        }
        return response()->json(['error' => 'could_not_create_token'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, HouseServiceInterface $houseService)
    {
        $list = $houseService->getById($id);
        return response()->json(compact('list'), 200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, HouseServiceInterface $houseService)
    {
        $inputs = $request->except(['image', '_method']);
        // $image = $request->file('images');
        $images = $request['image'];
        $result = $houseService->update($id, $inputs, $images);

        if($result){
            return response()->json(['result' => $result], 201);
        }
        return response()->json(['error' => 'could_not_create_token'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, HouseServiceInterface $houseService)
    {
        $result = $houseService->delete($id);
        if($result){
            return response()->json(null, 204);
        }
        return response()->json(['error' => 'could_not_create_token'], 500);
    }

    /**
     * Make your home favorite
     *
     * @param  int  $id
     * @return App\Contracts\HouseServiceInterface $houseService
     */

    public function favoriteHouse($id, HouseServiceInterface $houseService)
    {
        $result = $houseService->favorite($id);
        if($result) {
            return response()->json(['status' => 'success'], 201);
        }
        return response()->json(['error' => 'could not add favorite'], 500);
    }

    /**
     *  Display a listing of the favorite house.
     *
     * @return App\Contracts\HouseServiceInterface $houseService
     */

    public function favoriteHouseList(HouseServiceInterface $houseService)
    {
        $result = $houseService -> favoriteList();
        return response()->json(compact('result'), 200);
    }

}
