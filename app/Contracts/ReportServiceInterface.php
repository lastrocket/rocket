<?php

namespace App\Contracts;

use Laravel\Socialite\Contracts\User as ProviderUser;

interface ReportServiceInterface
{	
	public function addReport($inputs);

	public function getAllReports();

	public function answerReport($id, $input);
}