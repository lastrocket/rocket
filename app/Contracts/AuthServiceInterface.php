<?php

namespace App\Contracts;

use Laravel\Socialite\Contracts\User as ProviderUser;

interface AuthServiceInterface
{
    public function register($inputs);
    
    public function sendResetLinkEmail($input);
    
    public function resetPassword($inputs);

    public function getUserByEmail($email);

    public function checkToken($token);

    public function createOrGetUser($social, ProviderUser $providerUser);

    public function verification($email);

    public function verifyAccount($email);

}