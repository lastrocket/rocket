<?php

namespace App\Contracts;

interface HouseServiceInterface
{
    public function add($inputs, $images);

    public function getAll();

    public function update($id, $inputs, $images);

    public function delete($id);

    public function getById($id);

    public function favorite($id);

    public function favoriteList();

}