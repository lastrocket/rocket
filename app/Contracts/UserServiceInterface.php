<?php

namespace App\Contracts;

interface UserServiceInterface
{
	public function getAllUsers();

	public function addAdminByUserEmail($inputs);
	
	public function createUser($inputs);

	public function getUserById($id);

    public function update($id, $input, $image);

    public function deleteUserById($id);

	public function getMembersToken($id);

	public function countPerDay();
}