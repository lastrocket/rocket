<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Favorite extends Model
{
    use Notifiable;


    protected $table = 'favorites';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'house_id', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    	//
    ];

    public function house()
    {
        return $this->belongsTo('App\Models\House', 'house_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
