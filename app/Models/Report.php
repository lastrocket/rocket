<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use Notifiable;


    protected $table = 'reports';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'report', 'user_id', 'house_id', 'answer'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    	//
    ];

    public function house()
    {
        return $this->belongsTo('App\Models\House');
    }

    public function user(){
        return $this->belongsTo("App\User");
    }
}
