<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use Notifiable;


    protected $table = 'houses';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address', 'price', 'from_date', 'to_date', 'phone', 'rooms', 'type_of_rooms', 'rules', 'comfort', 'people_count', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    	//
    ];

    public function reports() {
        return $this->hasMany('App\Models\Report');
    }
    
    public function favorite()
    {
        return $this->hasOne('App\Models\Favorite');
    }

}
