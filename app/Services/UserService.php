<?php

namespace App\Services;

use App\Contracts\UserServiceInterface;
use App\User;
use JWTAuth;
use Mail;
use Visitor;
use Carbon\Carbon;
use App\Models\House;

class UserService implements UserServiceInterface
{
    public function getAllUsers() {
        return User::get();
    }

    public function addAdminByUserEmail($inputs) {
        $email = $inputs['email'];
        $user = User::where('email', $email)->first();
 
        if(!is_null($user)){
            $user->role_id = 2;
            $admin = $user->save();
            $data = [
                'password' => null,
            ];
            $data_pass = ['data' => $data];
            $this->sendAdminEmail($data_pass, $email);
         
        }else{
            $new_password = str_random(10);

            $admin = [
                'email' => $email,
                'password' => $new_password,
                'role_id' => 2, 
            ];

            $admin = User::create($admin);

            $data = [
                'password' => $new_password,
            ];
            $data_pass = ['data' => $data];
            $this->sendAdminEmail($data_pass, $email);            
        }
        
        return $admin;
    }

    public function createUser($inputs) {
        $inputs['password'] = bcrypt($inputs['password']);
        User::create($inputs);
        $user = User::where('email', $inputs['email'])->first();
        return $user;
    }

    public function getUserById($id) {
        return User::where('id', $id)->first();
    }
    /**
     * Update profile
     *
     * @param int       $id
     * @param array     $input 
     */
    public function update($id, $inputs, $image)
    {
        if($image != null){
            $destinationPath = public_path('/avatars');
            if (!is_dir($destinationPath)) {
                File::makeDirectory($destinationPath);
            }

            $inputs['image'] = time().'.'.$image->getClientOriginalExtension();
            $result = User::where('id', $id)->update($inputs);
            if($result) {
                $image->move($destinationPath, $inputs['image']);
            }
        }
        else {
            unset($inputs['image']);
            $result = User::where('id', $id)->update($inputs);
        }

        return $result;
    }

    public function deleteUserById($id) {
        return User::where('id', $id)->delete();
    }

    public function getMembersToken($id) {
        $user = $this->getUserById($id);
        $token = '';
        if($user) {
            $token = JWTAuth::fromUser($user);
        }
        return $token;
    }

    private function sendAdminEmail($data, $email)
    { 
        Mail::send('is_admin', $data, function($message) use ($email)
        {
            $message->to($email,'Summerhouse')->subject('test');
            $message->from(env('MAIL_USERNAME'));
        });
        if (Mail::failures()) {
            return false;
        }
        return true;
    }

    public function countPerDay()
    {
        $today = Carbon::today();

        $usersCount = User::whereBetween('created_at', [$today, $today->now()])->count();
        $housesCount = House::whereBetween('created_at', [$today, $today->now()])->count();
        $visitorsCount = Visitor::range($today, $today->now());
        $data = [
                'users_count' => $usersCount,
                'houses_count' => $housesCount,
                'visitors_count' => $visitorsCount,
        ];
        return $data;
    }
}