<?php

namespace App\Services;

use App\Models\Report;
use App\Contracts\ReportServiceInterface;
use Illuminate\Support\Facades\Auth;

class ReportService implements ReportServiceInterface
{
	public function addReport($inputs) {
		return Report::create($inputs);
	}

	public function delete($id) {
		return Report::where('id', $id)->delete();
	}

	public function getAllReports() {
		return Report::with(['house', 'user'])->get();
	}

	public function answerReport($id, $input) {
		$report = Report::find($id);
		if(!$report) {
			return false;
		}
		$report->answer = $input['answer'];
		$report->save();
		return $report;
	}
}
