<?php

namespace App\Services;

use App\User;
use App\Contracts\AuthServiceInterface;
use JWTFactory;
use JWTAuth;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Support\Facades\Auth;

class AuthService implements AuthServiceInterface
{
    public function register($inputs)
    {
        $inputs['password'] = bcrypt($inputs['password']);
        User::create($inputs);
        $user = User::where('email', $inputs['email'])->first();
        return $user;
    }

    public function getUserByEmail($email) {
    	return User::where('email', $email)->first();
    }

    public function getToken($input)
    {
        $input['token'] = strtolower(str_random(64));

        if(!DB::table('password_resets')->where('email', $input['email'])->exists()){
            DB::table('password_resets')->insert($input);
        } else {
			DB::table('password_resets')
				->where('email', $input['email'])
				->update($input);
        }
        return $input['token'];
    }

    public function sendResetLinkEmail($input)
    {  
        $token = $this->getToken($input);

		Mail::send('mail', ['token' => $token], function($message) use ($input)
		{
			$message->to($input['email'],'Summerhouse')->subject('test');
			$message->from(env('MAIL_USERNAME'));
		});
		if (Mail::failures()) {
	       	return false;
	    }
		return true;
    }

    public function checkToken($token) {
    	$result = DB::table('password_resets')
                    ->where('token', $token)
                    ->first();
        return $result;
    }

    public function resetPassword($inputs)
    {
        $inputs['password'] = bcrypt($inputs['password']);
        $user = User::where('email', $inputs['email'])->update($inputs);
        return $user;
    }

    public function createOrGetUser($social, $providerUser)
    {
        $account = SocialAccount::whereProvider($social)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $social
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }

    /**
     * Verification
     * 
     * @param array         $input
     */

    public function verification($email)
    {
        $encoded_email = base64_encode($email);

		Mail::send('activation', ['data' => $encoded_email], function($message) use ($email)
		{
			$message->to($email, 'Summerhouse')->subject('test');
			$message->from(env('MAIL_USERNAME'));
		});
		if (Mail::failures()) {
	       	return false;
        }
		return true;
    }

    /**
     * Activation
     * 
     * @param string            $confirmation
     */

    public function verifyAccount($email)
    {
        $user = User::where('email', base64_decode($email))->first();
        if ($user->is_verified !== 1){
            $user->is_verified = 1;
            $user->save();
            return true;
        }
        return false;
    }

}
