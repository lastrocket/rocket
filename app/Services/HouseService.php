<?php

namespace App\Services;

use File;
use App\User;
use App\Models\House;
use App\Models\Report;
use App\Models\Favorite;
use App\Models\HouseImage;
use Illuminate\Support\Facades\Auth;
use App\Contracts\HouseServiceInterface;

class HouseService implements HouseServiceInterface
{
    /**
     * Store a new house
     * 
     * @param array         $inputs
     * @param array          $images
     */
    public function add($inputs, $images) 
    {
        $inputs['user_id'] = Auth::id();
        if(is_null($images)){
            $house = House::create($inputs);
        } else{
            $house = House::create($inputs);
            $destinationPath = storage_path('app/public/house');
            if (!is_dir($destinationPath)) {
                File::makeDirectory($destinationPath);
            }
            if(is_array($images)){
                foreach ($images as $key => $value) {
                    $data['name'] = time() + $key . '.' . $value->getClientOriginalExtension();
                    $data['house_id'] = $house->id;
                    $image =  HouseImage::create($data);
                    if ($image) {
                        $value->move($destinationPath, $data['name']);
                    }
                }
            } else {
                $data['name'] = time() . '.' . $images->getClientOriginalExtension();
                $data['house_id'] = $house->id;
                $image =  HouseImage::create($data);
                if ($image) {
                    $images->move($destinationPath, $data['name']);
                }
            }
        }
        if($house) {
            return true;
        }
        return false;
    }
    
    /**
     *  Show the specified house
     * 
     * @param int           $id
     */

    public function getById($id)
    {
        return House::where('id', $id)->first();
    }

    /**
     * Get all the houses
     * 
     */

    public function getAll()
    {
        return House::all();
    }

    /**
     * Update the specified  house
     * 
     * @param int           $id
     * @param array         $inputs
     * @param array          $images
     */

    public function update($id, $inputs, $images)
    {
        $house = House::where('id', $id)->update($inputs);
        if(!is_null($images)){
            $destinationPath = storage_path('app/public/house/');
            if (!is_dir($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true);
            }

            if($this->deleteImages($id)) {
                HouseImage::where('house_id', $id)->delete(); 
            }

            foreach ($images as $key => $value) {
                $data['name'] = time() + $key . '.' . $value->getClientOriginalExtension();
                $data['house_id'] = $id;
                $image = HouseImage::create($data);
                if ($image) {
                    $value->move($destinationPath, $data['name']);
                }
            }
        }
        
        if($house){
            return true;
        }
        return false;
    }

    /**
     * Delete the specified house
     * 
     * @param int           $id
     */

    public function deleteImages($id) {
        $oldImages = HouseImage::where('house_id', $id)->pluck('name')->toArray();
        foreach ($oldImages as $key => $value) {
            if(file_exists(storage_path('app/public/house/') . $value)) {
                unlink(storage_path('app/public/house/') . $value);
            }
        }
        return true;
    }
    public function delete($id)
    {
        if($this->deleteImages($id)) {
           HouseImage::where('house_id', $id)->delete();
        }
        return House::where('id', $id)->delete();
    }

    public function favorite($id)
    {
        if(!House::find($id)){
            return false;
        }
        $user_id = Auth::id();
        Favorite::create(['house_id' => $id, 'user_id' => $user_id]);
        return true;
    }

    public function favoriteList()
    {
        $user = Auth::user();
        $favorites = $user->favorites()->with('house')->get();
        return $favorites;
    }

}
