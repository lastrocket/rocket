<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\HomeController;
use Visitor;
use Illuminate\Support\Facades\DB;

class Visitors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visitors';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Visitor count';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('visitor_registry')->delete(); 
    }
}